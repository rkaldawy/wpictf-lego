#Remy Kaldawy
from PIL import Image, ImageDraw
import random as rand

from lego import Brick

class QT():

    def __init__(self, boxes=None, center=None, width=None, quadrant=None):

        if boxes is None:
            self.children = [None for i in range(4)]
            return

        if center is None or width is None or quadrant is None:
            x_dist = max([bo[1][0] for br, bo in boxes.items()]) - min([bo[0][0] for br, bo in boxes.items()])
            z_dist = max([bo[1][2] for br, bo in boxes.items()]) - min([bo[0][2] for br, bo in boxes.items()])

            self.width = max([x_dist, z_dist])

            self.center = (min([bo[0][0] for br, bo in boxes.items()]) + x_dist/2, \
                           min([bo[0][2] for br, bo in boxes.items()]) + z_dist/2)

            self.width += 10

            self.number = -1
            #print(self.center, self.width)
        else:
            self.width = width/2
            if quadrant == 0:
                self.center = (center[0] + self.width / 2, center[1] - self.width / 2)
            elif quadrant == 1:
                self.center = (center[0] - self.width / 2, center[1] - self.width / 2)
            elif quadrant == 2:
                self.center = (center[0] - self.width / 2, center[1] + self.width / 2)
            elif quadrant == 3:
                self.center = (center[0] + self.width / 2, center[1] + self.width / 2)
            else:
                raise AttributeError

        self.children = [None for i in range(4)]


    def find_quadrant(self, mn, mx):
        if mx[0] < self.center[0] and mn[1] > self.center[1]:
            return 2
        elif mn[0] > self.center[0] and mn[1] > self.center[1]:
            return 3
        elif mx[0] < self.center[0] and mx[1] < self.center[1]:
            return 1
        elif mn[0] > self.center[0] and mx[1] < self.center[1]:
            return 0
        else:
            return -1

    def build_tree(self, boxes):
        children = [{} for i in range(4)]
        for brick, box in boxes.items():
            mn, mx = (box[0][0], box[1][2]), (box[1][0], box[0][2])

            q = self.find_quadrant(mn, mx)
            if q < 0:
                pass
            else:
                children[q][brick] = box

        for i in range(4):
            if len(children[i]) > 0:
                self.children[i] = QT(children[i], self.center, self.width, i)
                self.children[i].build_tree(children[i])

    # def print_node(self):
    #     print("My center is: " + str(self.center))
    #     print("My width is: " + str(self.width))
    #     for i in range(4):
    #         if self.children[i] is not None:
    #             self.children[i].print_node()

    # def get_fill_color(self, color):
    #     if color == Brick.Color.red.value:
    #         return (255, 0, 0, 0)
    #     elif color == Brick.Color.white.value:
    #         return (200, 200, 200, 0)
    #     elif color == Brick.Color.brown.value:
    #         return (90, 70, 55, 0)
    #     else:
    #         return (rand.randint(50, 200), rand.randint(50, 200), rand.randint(50, 200), 0)

    # def illustrate_node(self, d=None, prec=25):
    #     w, h = 600, 600
    #     map = Image.new('RGBA', (w, h), (255, 255, 255, 0))
    #
    #     # TODO: there might be something off with this
    #     tf = lambda x, z: (int(h / 2) + x, int(w / 2) + z)
    #
    #     root = False
    #     if d is None:
    #         d = ImageDraw.Draw(map)
    #         d.rectangle((w / 2 - 5, h / 2 - 5, w / 2 + 5, h / 2 + 5), fill=(0, 0, 0, 0))
    #         root = True
    #
    #     d_x, d_y = tf(self.center[0], self.center[1])
    #     corners = [(d_x-self.width/2, d_y-self.width/2), (d_x + self.width/2, d_y+self.width/2)]
    #
    #     if self.width < prec:
    #         d.rectangle(corners, fill=None, outline=(0, 0, 0, 0))
    #
    #     for i in range(4):
    #         if self.children[i] is not None:
    #             self.children[i].illustrate_node(d)
    #
    #     if root:
    #         map.save('quad_tree.bmp')

    def find_size(self, size):
        size += 1
        for i in range(4):
            if self.children[i] is not None:
                size = self.children[i].find_size(size)

        return size

    def assign_numbers(self, num):
        self.number = num
        num += 1
        for i in range(4):
            if self.children[i] is not None:
                num = self.children[i].assign_numbers(num)

        return num

    def export_tree_helper(self, tree, mx):
        leaf = []
        for i in range(4):
            if self.children[i] is not None:
                leaf += [self.children[i].number]
                tree = self.children[i].export_tree_helper(tree, mx)
            else:
                leaf += [mx + 1]
        tree[self.number-1] = leaf
        return tree


    def export_tree(self, dir="out_tree.txt", write_to_file = True):
        self.assign_numbers(1)

        tree = [None for i in range(0, self.find_size(0))]
        tree = self.export_tree_helper(tree, self.find_size(0))

        out = ""
        for node in tree:
            out += "".join([str(i) + " " for i in node])[:-1] + ", "

        if write_to_file:
            with open(dir, 'w+') as f:
                f.write(out)

        return out


    # def import_tree_helper(self, line):
    #     node = line[self.number - 1]
    #     for idx, elt in enumerate(node):
    #         if elt < len(line):
    #             self.children[idx] = QuadTreeNode([], self.center, self.width, idx)
    #             self.children[idx].number = elt
    #             self.children[idx].import_tree_helper(line)
    #         else:
    #             self.children[idx] = None


    # def import_tree(self, dir="out_tree.txt"):
    #     with open(dir, 'r') as f:
    #         line = f.read()
    #
    #     line = line.split(", ")[:-1]
    #     line = [[int(elt) for elt in node.split(" ")] for node in line]
    #     self.width = 500
    #     self.center = (0, 0)
    #     self.number = 1
    #
    #     self.import_tree_helper(line)
    #
    #     self.illustrate_node()

