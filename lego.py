# Zoraver Kang

from enum import Enum, IntEnum


class Brick(object):
    class Type(IntEnum):
        two_by_four = 3001
        two_by_four_smooth = 87079
        two_by_two = 3003
        one_by_one = 3005
        one_by_one_thin = 3024
        one_by_one_smooth = 3070
        two_by_one = 3004
        two_by_one_thin = 3023
        two_by_one_smooth = 3069
        two_by_one_special = 3794
        three_by_one_thin = 3623
        three_by_one_smooth = 63864
        four_by_one_thin = 3710
        four_by_one_smooth = 2431
        two_by_six_thin = 3034
        six_by_eight_thin = 3036

    #TODO: maybe implement in a class that extends dict?
    Dimensions = {
        #x, y, z
        Type.two_by_four.value: [32.0, 9.6, 16.0, 1],
        Type.two_by_four_smooth.value: [32.0, 3.2, 16.0, 0],

        Type.two_by_two.value: [16.0, 9.6, 16.0, 1],

        Type.two_by_one.value: [16.0, 9.6, 8.0, 1],
        Type.two_by_one_thin.value: [16.0, 3.2, 8.0, 1],
        Type.two_by_one_smooth.value: [16.0, 3.2, 8.0, 0],
        Type.two_by_one_special.value: [16.0, 3.2, 8.0, 1],

        Type.one_by_one.value: [8.0, 9.6, 8.0, 1],
        Type.one_by_one_thin.value: [8.0, 3.2, 8.0, 1],
        Type.one_by_one_smooth.value: [8.0, 3.2, 8.0, 0],

        Type.three_by_one_thin.value: [24.0, 3.2, 8.0, 1],
        Type.three_by_one_smooth.value: [24.0, 3.2, 8.0, 0],

        Type.four_by_one_thin.value: [32.0, 3.2, 8.0, 1],
        Type.four_by_one_smooth.value: [32.0, 3.2, 8.0, 0],

        Type.two_by_six_thin.value: [48.0, 3.2, 16.0, 1],
        Type.six_by_eight_thin.value: [64.0, 3.2, 48.0, 1]
    }

    class Color(IntEnum):
        white = 1
        red = 21
        blue = 23
        yellow = 24
        lime = 37
        brown = 192

    def __init__(self, color, matrix, variant):
        self.color = color
        self.matrix = matrix
        self.variant = variant

    #def __str__(self):
    #    return '%s, %d, %f, %f, %f, %f, %f, %f\n' % (
    #        self.variant.__str__(), self.color, self.position[0], self.position[1], self.position[2], \
    #        self.orientation[0], self.orientation[1], self.orientation[2])
