import hashlib
import sys
from binascii import hexlify, unhexlify

target = "3164e764a20097dcb6b1c2ba54510b2f"

def main():
    if len(sys.argv) < 2:
        print("Usage: command hash")
        return

    candidate = sys.argv[1]
    m = hashlib.md5()
    m.update(candidate.encode('utf-8'))

    data = m.digest()
    if hexlify(data).decode('utf-8') == target:
        print("Correct!")
    else:
        print("Wrong.")


if __name__ == "__main__":
    main()
