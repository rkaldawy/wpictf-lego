#Zoraver Kang and Remy Kaldawy

import xml.etree.ElementTree as ET
import numpy as np
import random as rand
import subprocess
from PIL import Image, ImageDraw
import itertools
import math

from lego import Brick
from QT import QT

def execute_command(cmd):
    ps = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    return ps.communicate()[0].decode('utf-8')

def parse_pose(pose):
    p = list(map(lambda cm: round(10 * cm, 1), pose[9:]))
    o = pose[:9]

    matrix = np.array(o)
    matrix.resize(3, 3)
    matrix[0][2] = -matrix[0][2]
    matrix[2][0] = -matrix[2][0]

    p = np.array(p)
    p.resize(3, 1)
    matrix = np.append(matrix, p, axis=1)

    r = np.array([0, 0, 0, 1])
    r.resize(1, 4)
    matrix = np.append(matrix, r, axis=0)

    return matrix


def parse_xml(path):
    parsed = []

    tree = ET.parse(path)
    root = tree.getroot()
    bricks = root.find('Bricks')
    for brick in bricks:
        part = brick.find('Part')
        bone = part.find('Bone')

        variant = Brick.Type(int(brick.attrib['designID']))
        color = int(part.attrib['materials'].split(',')[0])
        pose = bone.attrib['transformation']
        pose = list(map(float, pose.split(',')))

        parsed.append(Brick(color, parse_pose(pose), variant))

    return parsed


def get_starting_points(dims):
    cords = list(itertools.product(*zip([0, 0, 0], \
                                        [dims[0], dims[1], -dims[2]])))
    return [np.array(c) for c in cords]


def build_bounding_box(bricks):
    boxes = {}

    for b in bricks:
        points = get_starting_points(Brick.Dimensions[b.variant][:-1])
        m = b.matrix[0:3, 0:3]
        box = [np.full((1, 3), math.inf), np.full((1, 3), -math.inf)]

        for p in points:
            p_new = p + (np.array([-4, 0, 4]))
            n = m.dot(p_new)
            t = b.matrix[:,3][:-1]
            n += t
            box = [np.minimum(box[0], n), np.maximum(box[1], n)]
        boxes[b] = [box[0][0], box[1][0]]

    return boxes

# def draw_block(d, c):
#     #TODO: get all 12 linkages done
#     d.line([c[0], c[1]], fill=(0, 255, 0, 0), width=1)
#     d.line([c[1], c[2]], fill=(0, 255, 0, 0), width=1)
#     d.line([c[0], c[2]], fill=(0, 255, 0, 0), width=1)
#     d.line([c[2], c[3]], fill=(0, 255, 0, 0), width=1)
#     d.line([c[0], c[4]], fill=(0, 255, 0, 0), width=1)
#     d.line([c[6], c[7]], fill=(0, 255, 0, 0), width=1)
#     d.line([c[1], c[5]], fill=(0, 255, 0, 0), width=1)
#     pass

# def get_fill_color(brick):
#     if brick.color == Brick.Color.red.value:
#         return (255, 0, 0, 0)
#     elif brick.color == Brick.Color.white.value:
#         return (255, 255, 255, 0)
#     elif brick.color == Brick.Color.brown.value:
#         return (90, 70, 55, 0)
#     else:
#         return (rand.randint(50, 200), rand.randint(50, 200), rand.randint(50, 200), 0)


# def illustrate_boxes(boxes):
#     w, h = 600, 600
#     map = Image.new('RGBA', (w, h), (255, 255, 255, 0))
#
#     #TODO: there might be something off with this
#     tf = lambda x, z:(int(h/2)+x, int(w/2)+z)
#
#     d = ImageDraw.Draw(map)
#     d.rectangle((w/2-5, h/2-5, w/2+5, h/2+5), fill=(0, 0, 0, 0))
#     boxes_list = [(brick, box) for brick, box in boxes.items()]
#     boxes_list.sort(key=lambda r: r[1][0][1])
#
#     for brick, box in boxes_list:
#         #draw box
#         c = [tf(box[0][0], box[0][2]), tf(box[1][0], box[1][2])]
#         color = get_fill_color(brick)
#         d.rectangle(c, fill=color)
#
#         #draw outline of brick
#         m = brick.matrix[0:3, 0:3]
#         t = brick.matrix[:, 3][:-1]
#         points = get_starting_points(Brick.Dimensions[brick.variant][:-1])
#         points = [p + (np.array([-4, 0, 4])) for p in points]
#         c = [tf((m.dot(p) + t)[0], (m.dot(p) + t)[2]) for p in points]
#
#         draw_block(d, c)
#
#     map.save('test.bmp')


def build_quad_tree(boxes):
    root = QT(boxes)
    root.build_tree(boxes)
    #root.print_node()
    #root.illustrate_node()
    root.export_tree(dir="target_tree.txt")

    #imp = QuadTreeNode()
    #imp.import_tree()


#execute_command("./examine_legos.sh test.lxf")

#bricks = parse_xml("IMAGE100.LXFML")
#boxes = build_bounding_box(bricks)
#illustrate_boxes(boxes)
#build_quad_tree(boxes)